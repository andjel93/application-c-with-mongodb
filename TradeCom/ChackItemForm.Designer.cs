﻿namespace TradeCom
{
    partial class ChackItemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChackItemForm));
            this.dgwItem = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnChack = new System.Windows.Forms.Button();
            this.txbIDChack = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgwItem)).BeginInit();
            this.SuspendLayout();
            // 
            // dgwItem
            // 
            this.dgwItem.BackgroundColor = System.Drawing.Color.PaleGreen;
            this.dgwItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.sd,
            this.Column7});
            this.dgwItem.Location = new System.Drawing.Point(15, 51);
            this.dgwItem.Name = "dgwItem";
            this.dgwItem.Size = new System.Drawing.Size(844, 397);
            this.dgwItem.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Šifra";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Ime proizvoda";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Ime proizvođača";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Količina";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Cena";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Dobavljač";
            this.Column6.Name = "Column6";
            // 
            // sd
            // 
            this.sd.HeaderText = "Datum poslednje izmene";
            this.sd.Name = "sd";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Vrsta ambalaže";
            this.Column7.Name = "Column7";
            // 
            // btnChack
            // 
            this.btnChack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChack.Location = new System.Drawing.Point(190, 22);
            this.btnChack.Name = "btnChack";
            this.btnChack.Size = new System.Drawing.Size(157, 23);
            this.btnChack.TabIndex = 2;
            this.btnChack.Text = "Pronađi proizvod";
            this.btnChack.UseVisualStyleBackColor = true;
            this.btnChack.Click += new System.EventHandler(this.btnChack_Click);
            // 
            // txbIDChack
            // 
            this.txbIDChack.Location = new System.Drawing.Point(15, 25);
            this.txbIDChack.Name = "txbIDChack";
            this.txbIDChack.Size = new System.Drawing.Size(138, 20);
            this.txbIDChack.TabIndex = 3;
            this.txbIDChack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Šifra proizvoda:";
            // 
            // ChackItemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TradeCom.Properties.Resources.plavaPozadina;
            this.ClientSize = new System.Drawing.Size(869, 460);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbIDChack);
            this.Controls.Add(this.btnChack);
            this.Controls.Add(this.dgwItem);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChackItemForm";
            this.Text = "Provera stanja ";
            ((System.ComponentModel.ISupportInitialize)(this.dgwItem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgwItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn sd;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.Button btnChack;
        private System.Windows.Forms.TextBox txbIDChack;
        private System.Windows.Forms.Label label1;
    }
}