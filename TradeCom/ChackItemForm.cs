﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace TradeCom
{
    public partial class ChackItemForm : Form
    {
        public ChackItemForm()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void btnChack_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");


            if (txbIDChack.Text != "")
            {

                var query = Query.EQ("ID", int.Parse(txbIDChack.Text));
                var results = collection.Find(query).ToList();
                if (results.Count != 0)
                {

                    foreach (Items r in collection.Find(query))
                    {

                        DataGridViewRow row = (DataGridViewRow)dgwItem.Rows[0].Clone();

                        row.Cells[0].Value = r.ID;
                        row.Cells[1].Value = r.Ime_proizvoda;
                        row.Cells[2].Value = r.Ime_proizvodjaca;
                        row.Cells[3].Value = r.Kolicina;
                        row.Cells[4].Value = r.Cena;
                        row.Cells[5].Value = r.Dobavljac;
                        row.Cells[6].Value = r.Datum_poslednjeg_uvoza;
                        row.Cells[7].Value = r.Vrsta_ambalaze;
                        dgwItem.Rows.Add(row);


                    }

                }
                else
                {
                    MessageBox.Show("Ne postoji proizvod sa tom šifrom!");

                }
            }
            else
            {
                MessageBox.Show("Nije uneta šifra!");
            }

            
           


        }
    }
    
}
