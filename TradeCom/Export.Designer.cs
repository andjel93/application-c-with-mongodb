﻿namespace TradeCom
{
    partial class Export
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Export));
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.buttonAddArtical = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.listViewArtical = new System.Windows.Forms.ListView();
            this.columnID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNameOFProduct = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrices = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTotalPrices = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label3 = new System.Windows.Forms.Label();
            this.btnFinishBuy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(12, 42);
            this.textBoxID.MaxLength = 10;
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(193, 20);
            this.textBoxID.TabIndex = 0;
            this.textBoxID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxID_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Šifra proizvoda:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Količina:";
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(12, 81);
            this.textBoxAmount.MaxLength = 10;
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(193, 20);
            this.textBoxAmount.TabIndex = 2;
            this.textBoxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAmount_KeyPress);
            // 
            // buttonAddArtical
            // 
            this.buttonAddArtical.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddArtical.Location = new System.Drawing.Point(12, 122);
            this.buttonAddArtical.Name = "buttonAddArtical";
            this.buttonAddArtical.Size = new System.Drawing.Size(193, 44);
            this.buttonAddArtical.TabIndex = 4;
            this.buttonAddArtical.Text = "Dodaj proizvod ";
            this.buttonAddArtical.UseVisualStyleBackColor = true;
            this.buttonAddArtical.Click += new System.EventHandler(this.buttonAddArtical_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove.Location = new System.Drawing.Point(12, 229);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(193, 46);
            this.buttonRemove.TabIndex = 8;
            this.buttonRemove.Text = "Uloni proizvod sa liste";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // listViewArtical
            // 
            this.listViewArtical.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnID,
            this.columnNameOFProduct,
            this.columnAmount,
            this.columnPrices,
            this.columnTotalPrices});
            this.listViewArtical.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewArtical.FullRowSelect = true;
            this.listViewArtical.Location = new System.Drawing.Point(211, 26);
            this.listViewArtical.Name = "listViewArtical";
            this.listViewArtical.Size = new System.Drawing.Size(460, 395);
            this.listViewArtical.TabIndex = 9;
            this.listViewArtical.UseCompatibleStateImageBehavior = false;
            this.listViewArtical.View = System.Windows.Forms.View.Details;
            // 
            // columnID
            // 
            this.columnID.Text = "Šifra";
            this.columnID.Width = 75;
            // 
            // columnNameOFProduct
            // 
            this.columnNameOFProduct.Text = "Ime proizvoda";
            this.columnNameOFProduct.Width = 124;
            // 
            // columnAmount
            // 
            this.columnAmount.Text = "Količina";
            this.columnAmount.Width = 74;
            // 
            // columnPrices
            // 
            this.columnPrices.Text = "Cena";
            this.columnPrices.Width = 75;
            // 
            // columnTotalPrices
            // 
            this.columnTotalPrices.Text = "Ukupna cena";
            this.columnTotalPrices.Width = 104;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(9, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Selektuj količnu u listi i obriši:";
            // 
            // btnFinishBuy
            // 
            this.btnFinishBuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinishBuy.Location = new System.Drawing.Point(12, 375);
            this.btnFinishBuy.Name = "btnFinishBuy";
            this.btnFinishBuy.Size = new System.Drawing.Size(193, 46);
            this.btnFinishBuy.TabIndex = 11;
            this.btnFinishBuy.Text = "Izvrši kupovinu";
            this.btnFinishBuy.UseVisualStyleBackColor = true;
            this.btnFinishBuy.Click += new System.EventHandler(this.btnFinishBuy_Click);
            // 
            // Export
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TradeCom.Properties.Resources.plavaPozadina;
            this.ClientSize = new System.Drawing.Size(683, 431);
            this.Controls.Add(this.btnFinishBuy);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.listViewArtical);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonAddArtical);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxID);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Export";
            this.Text = "Prodaja robe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.Button buttonAddArtical;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.ListView listViewArtical;
        private System.Windows.Forms.ColumnHeader columnID;
        private System.Windows.Forms.ColumnHeader columnNameOFProduct;
        private System.Windows.Forms.ColumnHeader columnAmount;
        private System.Windows.Forms.ColumnHeader columnPrices;
        private System.Windows.Forms.ColumnHeader columnTotalPrices;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnFinishBuy;
    }
}