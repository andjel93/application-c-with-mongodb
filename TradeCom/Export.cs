﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;


namespace TradeCom
{
    public partial class Export : Form
    {
        public Export()
        {
            InitializeComponent();
        }

        private void buttonAddArtical_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");

            var query = Query.EQ("ID", int.Parse(textBoxID.Text));
            // MessageBox.Show(query.ToString());
           
            var results = collection.Find(query).ToList();


            if (results.Count == 0)
            {

                MessageBox.Show("Ne postoji proizvod sa tom šifrom!!!");
            }
            else
            {
                if (textBoxID.Text != "" && textBoxAmount.Text != "")
                {
                    foreach (Items p in collection.Find(query))
                    {
                        if (p.Kolicina >= int.Parse(textBoxAmount.Text))
                        {

                            ListViewItem it = new ListViewItem(Convert.ToString(p.ID));

                            it.SubItems.Add(p.Ime_proizvoda);
                            it.SubItems.Add(Convert.ToString(textBoxAmount.Text));
                            it.SubItems.Add(Convert.ToString(p.Cena));
                            double ukupnaCena;
                            ukupnaCena = p.Cena * double.Parse(textBoxAmount.Text);
                            it.SubItems.Add(Convert.ToString(ukupnaCena));
                            listViewArtical.Items.Add(it);

                            int quantityOfGoods = 0;
                            quantityOfGoods = p.Kolicina - int.Parse(textBoxAmount.Text);
                            var update = MongoDB.Driver.Builders.Update.Set("Kolicina", BsonValue.Create(quantityOfGoods));
                            collection.Update(query, update);
                        }
                        else
                        {
                            MessageBox.Show("Nedovoljna količina u magacinu!!!");
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Šifra ili količina nije uneta!!!");
                }
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");

            for (int i = 0; i < listViewArtical.SelectedItems.Count; i++)
            {
                int id = 0;
                id = int.Parse(listViewArtical.SelectedItems[i].Text);
                var query = Query.EQ("ID", id);
                foreach (Items p in collection.Find(query))
                {
                    int quantityOfGoods = 0;
                    quantityOfGoods = p.Kolicina + int.Parse(listViewArtical.SelectedItems[i].SubItems[2].Text);
                    var update = MongoDB.Driver.Builders.Update.Set("Kolicina", BsonValue.Create(quantityOfGoods));
                    collection.Update(query, update);


                }
            }
        
            foreach (ListViewItem eachItem in listViewArtical.SelectedItems)
            {
                
                listViewArtical.Items.Remove(eachItem);
            }
        }

        private void btnFinishBuy_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");

            int totalPrice = 0;

            for (int i = 0; i < listViewArtical.Items.Count; i++)
            {
                string colID= listViewArtical.Items[i].Text;
                string colAmount = listViewArtical.Items[i].SubItems[2].Text;
                string colTotalPrices = listViewArtical.Items[i].SubItems[4].Text;

                var query = Query.EQ("ID", int.Parse(colID));

                
               
                foreach(Items p in collection.Find(query))
                    {
            
                        totalPrice = totalPrice + int.Parse(colTotalPrices);
                 
                    }

            }



            if (totalPrice != 0)
            {
                MessageBox.Show("Ukupna cena robe je:" + totalPrice.ToString());
            }
            foreach (ListViewItem item in listViewArtical.Items)
            {
                listViewArtical.Items.Remove(item);
            }


        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void textBoxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }
    }
}
