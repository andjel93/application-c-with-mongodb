﻿namespace TradeCom
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buttonImport = new System.Windows.Forms.Button();
            this.buttonExport = new System.Windows.Forms.Button();
            this.btnChack = new System.Windows.Forms.Button();
            this.btnWriteOFF = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonImport
            // 
            this.buttonImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImport.Location = new System.Drawing.Point(84, 49);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(263, 42);
            this.buttonImport.TabIndex = 0;
            this.buttonImport.Text = "Unos robe u magacin";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonExport
            // 
            this.buttonExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExport.Location = new System.Drawing.Point(84, 97);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(263, 42);
            this.buttonExport.TabIndex = 1;
            this.buttonExport.Text = "Prodaja robe";
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // btnChack
            // 
            this.btnChack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnChack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChack.Location = new System.Drawing.Point(84, 145);
            this.btnChack.Name = "btnChack";
            this.btnChack.Size = new System.Drawing.Size(263, 42);
            this.btnChack.TabIndex = 2;
            this.btnChack.Text = "Provera stanja";
            this.btnChack.UseVisualStyleBackColor = true;
            this.btnChack.Click += new System.EventHandler(this.btnChack_Click);
            // 
            // btnWriteOFF
            // 
            this.btnWriteOFF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWriteOFF.Location = new System.Drawing.Point(84, 193);
            this.btnWriteOFF.Name = "btnWriteOFF";
            this.btnWriteOFF.Size = new System.Drawing.Size(263, 42);
            this.btnWriteOFF.TabIndex = 3;
            this.btnWriteOFF.Text = "Otpis pokvarene robe";
            this.btnWriteOFF.UseVisualStyleBackColor = true;
            this.btnWriteOFF.Click += new System.EventHandler(this.btnWriteOFF_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TradeCom.Properties.Resources.magacin;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(431, 297);
            this.Controls.Add(this.btnWriteOFF);
            this.Controls.Add(this.btnChack);
            this.Controls.Add(this.buttonExport);
            this.Controls.Add(this.buttonImport);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TradeCom";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.Button btnChack;
        private System.Windows.Forms.Button btnWriteOFF;
    }
}

