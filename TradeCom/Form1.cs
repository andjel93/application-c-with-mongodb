﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TradeCom
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            Import p = new Import();
            p.ShowDialog();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            Export ee = new Export();
            ee.ShowDialog();
        }

        private void btnWriteOFF_Click(object sender, EventArgs e)
        {
            RemoveForm rm = new RemoveForm();
            rm.ShowDialog();
        }

        private void btnChack_Click(object sender, EventArgs e)
        {
            ChackItemForm ci = new ChackItemForm();
            ci.ShowDialog();
        }
    }
}
