﻿namespace TradeCom
{
    partial class Import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Import));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNameOfItem = new System.Windows.Forms.TextBox();
            this.textBoxNameOfProducer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPrices = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSupplier = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPackage = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addItem = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.columID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columImeProizvoda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columProizvodjac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columKolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columCena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columDobavljac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columAmbalaza = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(24, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime proizvoda:";
            // 
            // textBoxNameOfItem
            // 
            this.textBoxNameOfItem.Location = new System.Drawing.Point(27, 67);
            this.textBoxNameOfItem.MaxLength = 50;
            this.textBoxNameOfItem.Name = "textBoxNameOfItem";
            this.textBoxNameOfItem.Size = new System.Drawing.Size(211, 20);
            this.textBoxNameOfItem.TabIndex = 1;
            // 
            // textBoxNameOfProducer
            // 
            this.textBoxNameOfProducer.Location = new System.Drawing.Point(27, 109);
            this.textBoxNameOfProducer.MaxLength = 50;
            this.textBoxNameOfProducer.Name = "textBoxNameOfProducer";
            this.textBoxNameOfProducer.Size = new System.Drawing.Size(211, 20);
            this.textBoxNameOfProducer.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(24, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ime proizvođača:";
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(27, 150);
            this.textBoxAmount.MaxLength = 10;
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(112, 20);
            this.textBoxAmount.TabIndex = 5;
            this.textBoxAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxAmount_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 4;
            // 
            // textBoxPrices
            // 
            this.textBoxPrices.Location = new System.Drawing.Point(27, 191);
            this.textBoxPrices.MaxLength = 10;
            this.textBoxPrices.Name = "textBoxPrices";
            this.textBoxPrices.Size = new System.Drawing.Size(112, 20);
            this.textBoxPrices.TabIndex = 7;
            this.textBoxPrices.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrices_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(24, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Količina:";
            // 
            // textBoxSupplier
            // 
            this.textBoxSupplier.Location = new System.Drawing.Point(27, 232);
            this.textBoxSupplier.MaxLength = 50;
            this.textBoxSupplier.Name = "textBoxSupplier";
            this.textBoxSupplier.Size = new System.Drawing.Size(211, 20);
            this.textBoxSupplier.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(24, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Cena:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(24, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Dobavljač:";
            // 
            // textBoxPackage
            // 
            this.textBoxPackage.Location = new System.Drawing.Point(27, 314);
            this.textBoxPackage.MaxLength = 50;
            this.textBoxPackage.Name = "textBoxPackage";
            this.textBoxPackage.Size = new System.Drawing.Size(172, 20);
            this.textBoxPackage.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(24, 255);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(177, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Datum poslednje nabavke:";
            // 
            // addItem
            // 
            this.addItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addItem.Location = new System.Drawing.Point(50, 359);
            this.addItem.Name = "addItem";
            this.addItem.Size = new System.Drawing.Size(141, 40);
            this.addItem.TabIndex = 14;
            this.addItem.Text = "Dodaj";
            this.addItem.UseVisualStyleBackColor = true;
            this.addItem.Click += new System.EventHandler(this.addItem_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(24, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "Šifra proizvoda:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(27, 28);
            this.textBoxPassword.MaxLength = 10;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(131, 20);
            this.textBoxPassword.TabIndex = 15;
            this.textBoxPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPassword_KeyPress);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.PaleGreen;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columID,
            this.columImeProizvoda,
            this.columProizvodjac,
            this.columKolicina,
            this.columCena,
            this.columDobavljac,
            this.columDate,
            this.columAmbalaza});
            this.dataGridView1.Location = new System.Drawing.Point(264, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(831, 435);
            this.dataGridView1.TabIndex = 17;
            // 
            // columID
            // 
            this.columID.HeaderText = "Šifra";
            this.columID.Name = "columID";
            // 
            // columImeProizvoda
            // 
            this.columImeProizvoda.HeaderText = "Proizvod";
            this.columImeProizvoda.Name = "columImeProizvoda";
            // 
            // columProizvodjac
            // 
            this.columProizvodjac.HeaderText = "Proizvođač";
            this.columProizvodjac.Name = "columProizvodjac";
            // 
            // columKolicina
            // 
            this.columKolicina.HeaderText = "Količina [Kg]";
            this.columKolicina.Name = "columKolicina";
            // 
            // columCena
            // 
            this.columCena.HeaderText = "Cena [RSD]";
            this.columCena.Name = "columCena";
            // 
            // columDobavljac
            // 
            this.columDobavljac.HeaderText = "Dobavljač";
            this.columDobavljac.Name = "columDobavljac";
            // 
            // columDate
            // 
            this.columDate.HeaderText = "Datum poslednje nabavke";
            this.columDate.Name = "columDate";
            // 
            // columAmbalaza
            // 
            this.columAmbalaza.HeaderText = "Ambalaža";
            this.columAmbalaza.Name = "columAmbalaza";
            // 
            // textBoxDate
            // 
            this.textBoxDate.CustomFormat = "dd-MM-yyyy";
            this.textBoxDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.textBoxDate.Location = new System.Drawing.Point(27, 273);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(112, 21);
            this.textBoxDate.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(24, 296);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "Vrsta ambalaže:";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdate.Location = new System.Drawing.Point(50, 405);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(141, 40);
            this.buttonUpdate.TabIndex = 20;
            this.buttonUpdate.Text = "Ažuriraj";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // Import
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::TradeCom.Properties.Resources.plavaPozadina;
            this.ClientSize = new System.Drawing.Size(1107, 457);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxDate);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.addItem);
            this.Controls.Add(this.textBoxPackage);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxSupplier);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxPrices);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxAmount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxNameOfProducer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNameOfItem);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Import";
            this.Text = "Dodavanje i ažuriranje robe";
            this.Load += new System.EventHandler(this.Import_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNameOfItem;
        private System.Windows.Forms.TextBox textBoxNameOfProducer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPrices;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxSupplier;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPackage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button addItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker textBoxDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn columID;
        private System.Windows.Forms.DataGridViewTextBoxColumn columImeProizvoda;
        private System.Windows.Forms.DataGridViewTextBoxColumn columProizvodjac;
        private System.Windows.Forms.DataGridViewTextBoxColumn columKolicina;
        private System.Windows.Forms.DataGridViewTextBoxColumn columCena;
        private System.Windows.Forms.DataGridViewTextBoxColumn columDobavljac;
        private System.Windows.Forms.DataGridViewTextBoxColumn columDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn columAmbalaza;
    }
}