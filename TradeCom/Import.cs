﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace TradeCom
{
    public partial class Import : Form
    {
        public Import()
        {
            InitializeComponent();
        }

        private void grid()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");
            dataGridView1.Rows.Clear();
            foreach (Items r in collection.FindAll())
            {

                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();

                row.Cells[0].Value = r.ID;
                row.Cells[1].Value = r.Ime_proizvoda;
                row.Cells[2].Value = r.Ime_proizvodjaca;
                row.Cells[3].Value = r.Kolicina;
                row.Cells[4].Value = r.Cena;
                row.Cells[5].Value = r.Dobavljac;
                row.Cells[6].Value = r.Datum_poslednjeg_uvoza;
                row.Cells[7].Value = r.Vrsta_ambalaze;
                dataGridView1.Rows.Add(row);


            }

        }


        private void addItem_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");
            if (textBoxPassword.Text != "" && textBoxNameOfItem.Text != "" && textBoxNameOfProducer.Text != "" && textBoxAmount.Text != ""
             && textBoxPrices.Text != "" && textBoxSupplier.Text != "" && textBoxDate.Text != "" && textBoxPackage.Text != "")
            {
               
                    var query = Query.EQ("ID", int.Parse(textBoxPassword.Text));


                    var results = collection.Find(query).ToList();


                    if (results.Count == 0)
                    {
                        Items item = new Items
                        {
                            ID = int.Parse(textBoxPassword.Text),
                            Ime_proizvoda = textBoxNameOfItem.Text,
                            Ime_proizvodjaca = textBoxNameOfProducer.Text,
                            Kolicina = int.Parse(textBoxAmount.Text),
                            Cena = double.Parse(textBoxPrices.Text),
                            Dobavljac = textBoxSupplier.Text,
                            Datum_poslednjeg_uvoza = textBoxDate.Text,
                            Vrsta_ambalaze = textBoxPackage.Text,
                        };

                        collection.Insert(item);
                        grid();


                        MessageBox.Show("Roba je dodata!");
                    }
                    else
                    {

                        MessageBox.Show("Već postoji roba sa tom šifrom! Ažuriraj robu ili upiši drugu šifru!");
                    }
                
            }

            else
            {
                MessageBox.Show("Nisu uneta sva polja!");
            }
            
            
            
        }

        private void Import_Load(object sender, EventArgs e)
        {
            grid();
           
        }


        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");

            var query = Query.EQ("ID", int.Parse(textBoxPassword.Text));
  


            var results = collection.Find(query).ToList();


            if (results.Count != 0)
            {

                if (textBoxNameOfItem.Text != "")
                {
                    var update = MongoDB.Driver.Builders.Update.Set("Ime_proizvoda", BsonValue.Create(textBoxNameOfItem.Text));
                    collection.Update(query, update);
                };

                if (textBoxNameOfProducer.Text != "")
                {
                    var update = MongoDB.Driver.Builders.Update.Set("Ime_proizvodjaca", BsonValue.Create(textBoxNameOfProducer.Text));
                    collection.Update(query, update);
                };

                if (textBoxAmount.Text != "")
                {
                    foreach (Items p in collection.Find(query))
                    {
                        int quantityOfGoods = 0;
                        quantityOfGoods = p.Kolicina + int.Parse(textBoxAmount.Text);
                           
                        var update = MongoDB.Driver.Builders.Update.Set("Kolicina", BsonValue.Create(quantityOfGoods));
                        collection.Update(query, update);
                        MessageBox.Show("Dodata je količina:  " + int.Parse(textBoxAmount.Text) +"Kg" + "   Trenutna količina robe " + p.Ime_proizvoda + " je:  " + quantityOfGoods + "Kg");
                    }
                      
                };

                if (textBoxPrices.Text != "")
                {
                    var update = MongoDB.Driver.Builders.Update.Set("Cena", BsonValue.Create(textBoxPrices.Text));
                    collection.Update(query, update);
                };

                if (textBoxSupplier.Text != "")
                {
                    var update = MongoDB.Driver.Builders.Update.Set("Dobavljac", BsonValue.Create(textBoxSupplier.Text));
                    collection.Update(query, update);
                };

                if (textBoxDate.Text != "")
                {
                    var update = MongoDB.Driver.Builders.Update.Set("Datum_poslednjeg_uvoza", BsonValue.Create(textBoxDate.Text));
                    collection.Update(query, update);
                };

                if (textBoxPackage.Text != "")
                {
                    var update = MongoDB.Driver.Builders.Update.Set("Vrsta_ambalaze", BsonValue.Create(textBoxPackage.Text));
                    collection.Update(query, update);
                    
                };
                grid();
                MessageBox.Show("Ažuriranje robe je uspešno izvršeno!");
            }
           else
            {

                MessageBox.Show("Ne postoji roba sa tom šifrom!");
            }


        }

        private void textBoxPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if(!Char.IsDigit(ch) && ch!=8  && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void textBoxAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void textBoxPrices_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }
    }
  
}
