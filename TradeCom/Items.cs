﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace TradeCom
{
    class Items
    {
        public ObjectId Id { get; set; }
        public int ID{ get; set; }
        public string Ime_proizvoda { get; set; }
        public string  Ime_proizvodjaca{ get; set; }
        public int Kolicina { get; set; }
        public double Cena { get; set; }
        public string Dobavljac{ get; set; }
        public string Datum_poslednjeg_uvoza { get; set; }
        public string Vrsta_ambalaze  { get; set; }

    }
}
