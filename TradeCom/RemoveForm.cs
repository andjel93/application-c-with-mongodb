﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace TradeCom
{
    public partial class RemoveForm : Form
    {
        public RemoveForm()
        {
            InitializeComponent();
        }
        private void grid()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");
            dataGridView1.Rows.Clear();
            foreach (Items r in collection.FindAll())
            {

                DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();

                row.Cells[0].Value = r.ID;
                row.Cells[1].Value = r.Ime_proizvoda;
                row.Cells[2].Value = r.Ime_proizvodjaca;
                row.Cells[3].Value = r.Kolicina;
                row.Cells[4].Value = r.Cena;
                row.Cells[5].Value = r.Dobavljac;
                row.Cells[6].Value = r.Datum_poslednjeg_uvoza;
                row.Cells[7].Value = r.Vrsta_ambalaze;
                dataGridView1.Rows.Add(row);


            }

        }

        private void btnWriteOffQuant_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");


            if (txbID.Text != "" && txbAmount.Text != "")
            {

                var query = Query.EQ("ID", int.Parse(txbID.Text));
                var results = collection.Find(query).ToList();
                if (results.Count != 0)
                {

                    foreach (Items p in collection.Find(query))
                    {
                        if (p.Kolicina> int.Parse(txbAmount.Text))
                        {
                            int quantityOfGoods = 0;
                            quantityOfGoods = p.Kolicina - int.Parse(txbAmount.Text);
                            var update = MongoDB.Driver.Builders.Update.Set("Kolicina", BsonValue.Create(quantityOfGoods));
                            collection.Update(query, update);
                            MessageBox.Show("Otpis je izvršen! Trenutna količina artikla je" + quantityOfGoods);
                        }
                        else
                        {
                            MessageBox.Show("GREŠKA: Količina za otpis je veća od količine u magacinu!");
                        }
                    }

                }
                else
                {
                    MessageBox.Show("Ne postoji proizvod sa tom šifrom!");

                }
            }
            else
            {
                MessageBox.Show("Nisu uneta sva polja!");
            }

            grid();


        }


        private void RemoveForm_Load(object sender, EventArgs e)
        {
            grid();
        }

        private void txbID_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void txbAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void txbIDArtikal_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void btnWriteOffItem_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connectionString);
            var db = server.GetDatabase("TradeCom");

            var collection = db.GetCollection<Items>("Proizvod");


            if (txbIDArtikal.Text != "")
            {

                var query = Query.EQ("ID", int.Parse(txbIDArtikal.Text));
                var results = collection.Find(query).ToList();
                if (results.Count != 0)
                {

                   collection.Remove(query);
              
                }
                else
                {
                    MessageBox.Show("Ne postoji proizvod sa tom šifrom!");

                }
            }
            else
            {
                MessageBox.Show("Nije uneta šifra!");
            }

            grid();

        }
    }
}
